import { initialize, renderProjects } from './displayController';

const run = () => {
    $(
        _ => {
            initialize();
            renderProjects();
        }
    );
}

export { run };