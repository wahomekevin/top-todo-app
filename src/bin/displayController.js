import { loadProjects, loadTasks, saveProject, saveTask } from './storage';

const closeNewProjectModal = _ => {
    $("#projectName").val("");
    $("#projectDescription").val("");
    $('#newProject').modal('toggle');
}

const closeNewTaskModal = _ => {
    $("#taskName").val("");
    $("#taskDescription").val("");
    $("#taskDueDate").val("");
    $("#taskPriority").val("");
    $('#newTask').modal('toggle');
}

const initialize = _ => {

    $("#newProjectButton").click( event => {
        const name = $("#projectName").val();
        const description = $("#projectDescription").val();
        saveProject(name, description);
        closeNewProjectModal();
        setTimeout( _ => renderProjects(), 500 ); 
    });

    $("#newTaskButton").click( event => {
        const name = $("#taskName").val();
        const description = $("#taskDescription").val();
        const dueDate = $("#taskDueDate").val();
        const priority = $("#taskPriority").val();
        const projectId = $(event.currentTarget).data("id");
        saveTask(projectId, name, description, dueDate, priority);
        closeNewTaskModal();
        setTimeout( _ => renderProjects(), 500 ); 
    });

    $("#closeButton").click( event => {
        closeNewProjectModal();
    });

    $("#closeTaskButton").click( event => {
        closeNewTaskModal();
    });
}

const renderProjects = _  => {
    const projects = loadProjects();
    const projectsElement = $('#projects');
    const projectsList = projects.map((project, index) => {
        return `
                <div class="project col-xs-12">
                    <h3>${ project.title }</h3>
                    <p class="description">${ project.description }</p>
                    <div class="bottom">
                        <button data-id="${ index }" class="btn btn-sm ${ project.tasks.length > 0 ? "btn-primary": "" }"><span class="tasks"><i class="fas fa-tasks"></i> ${ project.tasks === undefined ? null : project.tasks.length || "0" } </span></button>
                        <button data-id="${ index }" class="btn btn-sm pull-right add-task">Add Task</button>
                    </div>
                </div>`;
    });
    
    projectsElement.empty();
    projectsElement.append(projectsList);

    $('button[data-id].btn-primary').click( event => {
        event.stopPropagation();
        const projectId =  $(event.currentTarget).data( "id" );
        const tasks = loadTasks(projectId);
        renderTasks(tasks, projectId);
        setTimeout( _ => $('#viewTasks').modal(), 500 );        
    });

    $('button[data-id].add-task').click( event => {
        event.stopPropagation();
        const projectId =  $(event.currentTarget).data( "id" );
        $("#newTaskButton").attr( "data-id",  projectId);
        $('#newTask').modal();              
    });
};

const renderTasks = (tasks, projectId) => {
    const tasksElement = $('#tasks');
    tasksElement.empty();

    const tasksList = tasks.map( (task, index) => {
        return `
            <div data-id="${ index } data-pid="${ projectId }" class="row task">
                <div class="col-lg-7 title">
                    <h5>${ index + 1 }. ${ task.name }</h5>                            
                </div>
                <div class="col-lg-5 pull-right">
                    <p class="priority">Priority ${ task.priority } - <span class="due"> ${ task.dueDate } </span></p>
                </div>
            </div>`;
    });
    tasksElement.append(tasksList);
};

export { initialize, renderProjects };