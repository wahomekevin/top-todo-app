import { create as createProject } from '../lib/projectFactory';
import { create as createTask } from '../lib/taskFactory';

let sampleProjects = Array.from([createProject("Sample project", "This is just a sample project.",
    createTask("Task 1", "Interesting description 1", "1st Jan 2019", "A"),
    createTask("Task 2", "Interesting description 2", "1st Jan 2020", "D"),
    createTask("Task 3", "Interesting description 3", "1st Jan 2021", "C"),
    createTask("Task 4", "Interesting description 4", "1st Jan 2022", "B"))
]);

const deleteTask = (projectId, taskId) => {
    delete projects[projectId].tasks[taskId];
}
const loadProjects = () => {
    const localProjects = JSON.parse(localStorage.getItem("projects"));
    return localProjects === null ? sampleProjects : localProjects;
};
const loadTasks = projectId => {
    const projects = loadProjects();
    return projects[projectId].tasks;
};
const saveProject = (name, description) => {
    const project = createProject(name, description);
    const projects = loadProjects();
    projects.push(project);
    localStorage.setItem("projects", JSON.stringify(projects));
    return project;
}
const saveTask = (id, name, description, dueDate, priority) => {
    const newTask = createTask(name, description, dueDate, priority);
    const projects = loadProjects();
    projects[id].tasks.push(newTask);
    localStorage.setItem("projects", JSON.stringify(projects));
    return newTask;
}

export { initialize, deleteTask, loadProjects, loadTasks, saveProject, saveTask };