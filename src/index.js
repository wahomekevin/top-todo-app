// app styles
require('./styles/main.scss');

// app
import { run } from "./bin/app";

run();