const create = (title, description, ...tasks) => {
    return { title, description, tasks };
}

export { create };