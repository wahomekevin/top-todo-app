const create = (name, description, dueDate, priority) => {
    return { name, description, dueDate, priority };
};

export { create };